﻿unit TestJSONReader;

interface

uses
  TestFramework,
  Generics.Collections,
  SysUtils,
  JSON.Reader;

type
  // Testmethoden für Klasse IJSONReader

  TestIJSONReader = class(TTestCase)
  strict private
    FIJSONReader: IJSONReader;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestSimpleObject;
    procedure TestSimpleArray;
    procedure TestNestedObjects;
    procedure TestNestedArrays;
    procedure TestComplicatedObject;
    procedure TestComplicatedArray;
    procedure TestPaddedObject;
    procedure TestInvalidObject;
    procedure TestStringWithBraces;
    procedure TestStringWithCurlyBrace;
    procedure TestStringWithSlashes;
    procedure TestStringWithUnicode;
    procedure TestInvalidUnicode;
    procedure TestBigString;
    procedure TestDeepArray;
    procedure TestDeepObject;
    procedure TestKeyWithoutQuotes;
  end;

implementation

uses
  JSON, IOUtils;

procedure TestIJSONReader.SetUp;
begin
  FIJSONReader := getJSONReader;
end;

procedure TestIJSONReader.TearDown;
begin
  FIJSONReader := nil;
end;

procedure TestIJSONReader.TestSimpleObject;
var
  ReturnValue: IJSONObject;
  aText: string;
begin
  aText := '{"IntValue":123}';
  ReturnValue := FIJSONReader.readObject(aText);

  CheckEquals(ReturnValue.GetInteger('IntValue'), 123);
end;

procedure TestIJSONReader.TestStringWithSlashes;
var
  jo: IJSONObject;
  s: string;
begin
  jo := FIJSONReader.readObject('{"special":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf\/\/\/7\/\/9\/\/\/AP\/+AA\/\/\/\/\/+AAAB\/\/\/"}');
  s := jo.GetString('special');
  Check(s = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf///7//9///AP/+AA/////+AAAB///');
end;

procedure TestIJSONReader.TestStringWithUnicode;
var
  jo: IJSONObject;
  s: string;
begin
  jo := FIJSONReader.readObject('{"unicode":"\u0021"}');
  s := jo.GetString('unicode');
  Check(s = '!');
end;

procedure TestIJSONReader.TestStringWithBraces;
var
  ReturnValue: IJSONObject;
  aText: string;
begin
  aText := '{"String":"(empty)"}';
  ReturnValue := FIJSONReader.readObject(aText);

  CheckEquals(ReturnValue.GetString('String'), '(empty)');
end;

procedure TestIJSONReader.TestBigString;
var
  s : string;
  ReturnValue: IJSONObject;
begin
  s := TFile.ReadAllText('youtube.txt');
  ReturnValue := FIJSONReader.readObject(s);
  // TODO : Check for some values
end;

procedure TestIJSONReader.TestComplicatedArray;
var
  ReturnValue: IJSONArray;
  aText: string;
begin
  aText := '[{"IntValue":123},"abc",true,[1,2,3]]';
  ReturnValue := FIJSONReader.readArray(aText);

  CheckEquals(ReturnValue.Count, 4);
  CheckEquals(ReturnValue.GetJSONObject(0).GetInteger('IntValue'), 123);
  CheckEquals(ReturnValue.GetString(1), 'abc');
  CheckEquals(ReturnValue.GetBoolean(2), true);
  CheckEquals(ReturnValue.GetJSONArray(3).Count, 3);
end;

procedure TestIJSONReader.TestComplicatedObject;
var
  ReturnValue: IJSONObject;
  aText: string;
begin
  aText := '{"IntValue":123,"obj":{"boolValue":false},"array":[1.23,true]}';
  ReturnValue := FIJSONReader.readObject(aText);

  CheckEquals(123, ReturnValue.GetInteger('IntValue'));
  CheckEquals(false, ReturnValue.GetJSONObject('obj').GetBoolean('boolValue'));
  CheckEquals(2, ReturnValue.GetJSONArray('array').Count);
end;

procedure TestIJSONReader.TestDeepArray;
var
  aText: string;
  ja, jaInner: IJSONArray;
begin
  aText := '[[[[[[0,[1,2]]],3]]]';
  ja := FIJSONReader.readArray(aText);
  jaInner := ja.GetJSONArray(0).GetJSONArray(0).GetJSONArray(0).GetJSONArray(0).GetJSONArray(0);
  CheckEquals(0, jaInner.GetInteger(0));
  CheckEquals(1, jaInner.GetJSONArray(1).GetInteger(0));
  CheckEquals(2, jaInner.GetJSONArray(1).GetInteger(1));
  jaInner := ja.GetJSONArray(0).GetJSONArray(0).GetJSONArray(0);
  CheckEquals(3, jaInner.GetInteger(1));
end;

procedure TestIJSONReader.TestDeepObject;
var
  aText: string;
  jo, joInner: IJSONObject;
begin
  aText := '{"o":{"o":{"o":{"o":{"key":42}}},"o2":{"key":"answer"}}}';
  jo := FIJSONReader.readObject(aText);
  joInner := jo.GetJSONObject('o').GetJSONObject('o').GetJSONObject('o').GetJSONObject('o');
  CheckEquals(42, joInner.GetInteger('key'));
  joInner := jo.GetJSONObject('o').GetJSONObject('o2');
  CheckEquals('answer', joInner.GetString('key'));
end;

procedure TestIJSONReader.TestStringWithCurlyBrace;
var
  aText: string;
  jo: IJSONObject;
begin
  aText := '{"key":"ab}cdef}"}';
  jo := FIJSONReader.readObject(aText);
  CheckEquals('ab}cdef}', jo.GetString('key'));
end;

procedure TestIJSONReader.TestInvalidObject;
var
  aText: string;
begin
  aText := '{"value":1'; // missing "}" at end
  ExpectedException := JSONException;
  FIJSONReader.readObject(aText);
end;

procedure TestIJSONReader.TestInvalidUnicode;
var
  aText: string;
begin
  aText := '{"value":"\umoep"}'; // missing "}" at end
  ExpectedException := JSONException;
  FIJSONReader.readObject(aText);
end;

procedure TestIJSONReader.TestKeyWithoutQuotes;
var
  aText: string;
  jo: IJSONObject;
begin
  aText := '{key:123}';
  ExpectedException := JSONException;
  jo := FIJSONReader.readObject(aText);
end;

procedure TestIJSONReader.TestNestedArrays;
var
  ReturnValue: IJSONArray;
  aText: string;
begin
  aText := '[[1,2],[3],[]]';
  ReturnValue := FIJSONReader.readArray(aText);
  CheckEquals(3, ReturnValue.Count);
  CheckEquals(1, ReturnValue.GetJSONArray(0).GetInteger(0));
  CheckEquals(1, ReturnValue.GetJSONArray(1).Count);
  CheckEquals(0, ReturnValue.GetJSONArray(2).Count);
end;

procedure TestIJSONReader.TestNestedObjects;
var
  ReturnValue: IJSONObject;
  aText: string;
  keys: TArray<string>;
begin
  aText := '{"obj1":{"int":0},"obj2":{"obj2.1":{"string":"def"}},"emptyObject":{}}';
  ReturnValue := FIJSONReader.readObject(aText);

  CheckEquals(0, ReturnValue.GetJSONObject('obj1').GetInteger('int'));
  CheckEquals('def', ReturnValue.GetJSONObject('obj2').GetJSONObject('obj2.1').GetString('string'));

  keys := ReturnValue.GetJSONObject('emptyObject').GetKeys;
  CheckEquals(0, length(keys));
end;

procedure TestIJSONReader.TestPaddedObject;
var
  ReturnValue: IJSONObject;
  aText: string;
begin
  aText := '  {' + #13#10 + '  "key" : "value" ' + #13#10 + '}  ';
  ReturnValue := FIJSONReader.readObject(aText);
  CheckEquals(ReturnValue.GetString('key'), 'value');
end;

procedure TestIJSONReader.TestSimpleArray;
var
  ReturnValue: IJSONArray;
  aText: string;
  expectedDouble: double;
begin
  aText := '["abc",1,5.67]';
  ReturnValue := FIJSONReader.readArray(aText);

  CheckEquals(3, ReturnValue.Count, 'element count');
  CheckEquals('abc', ReturnValue.GetString(0), 'element[0]');
  CheckEquals(1, ReturnValue.GetInteger(1), 'element[1]');
  //
  expectedDouble := 5.67;
  CheckEquals(ReturnValue.GetDouble(2), expectedDouble, 'element[2]');
end;

initialization

// Alle Testfälle beim Testprogramm registrieren
RegisterTest(TestIJSONReader.Suite);

end.
