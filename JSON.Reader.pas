unit JSON.Reader;

interface

uses
  JSON;

type
  IJSONReader = interface
    ['{19C800E9-95F4-40BC-A87C-FE0E4C0DF4BE}']
    function readObject(const aText: string): IJSONObject;
    function readArray(const aText: string): IJSONArray;
  end;

function getJSONReader: IJSONReader;

implementation

uses
  System.SysUtils,
  Windows,
  Generics.Collections,
  JSON.Classes,
  JSON.IOHelper;

type
  TJSONType = (jtString, jtInteger, jtDouble, jtBoolean, jtObject, jtArray, jtNull);

  TJSONReader = class(TInterfacedObject, IJSONReader)
  private
    fTokens: TList<string>;
    function Tokenize(aText: string): TList<string>;
    function isControlCharacter(aChar: Char): boolean;
  protected
    function isObject(const aText: string): boolean;
    function isArray(const aText: string): boolean;
    function isString(const aText: string): boolean;
    function isBoolean(const aText: string): boolean;
    function isInteger(const aText: string): boolean;
    function isDouble(const aText: string): boolean;
    function isNull(const aText: string): boolean;

    function getType(const aText: string): TJSONType;
    function unescapeString(s: string): string;
    function StrToFloatF(const aText: string): double;
    function StripQuotes(const aText: string): string;

    function ObjectFromToken(lo, hi: integer): IJSONObject;
    function ArrayFromToken(lo, hi: integer): IJSONArray;

    function getClosingTokenIndex(aIndex: integer): integer;
    function findNextElement(aIndex: integer): integer;
    function findQuotationMark(const aText: string; aIndex: integer): integer;
  public
    function readObject(const aText: string): IJSONObject;
    function readArray(const aText: string): IJSONArray;
  end;

function getJSONReader: IJSONReader;
begin
  result := TJSONReader.Create;
end;

{ TJSONReader }

function TJSONReader.unescapeString(s: string): string;
var
  i: integer;
  hex: string;
  sb: TStringBuilder;
begin
  sb := TStringBuilder.Create;
  try
    i := 1;
    while i <= length(s) do
    begin
      if s[i] = '\' then
      begin
        inc(i);
        case s[i] of
          '"':
            sb.Append('"');
          '\':
            sb.Append('\');
          '/':
            sb.Append('/');
          'b':
            sb.Append(#08);
          'f':
            sb.Append(#12);
          'n':
            sb.Append(#10);
          'r':
            sb.Append(#13);
          't':
            sb.Append(#09);
          'u':
            begin
              inc(i);
              hex := '$' + copy(s, i, 4);
              try
                sb.Append(WideChar(StrToInt(hex)));
              except
                on ex: EConvertError do
                begin
                  ex.RaiseOuterException(JSONException.Create('Invalid Unicode found: ' + s));
                end;
              end;
              inc(i, 3);
            end
        else
          raise JSONException.Create('Invalid escape character inside: ' + s);
        end;
      end
      else
        sb.Append(s[i]);
      inc(i);
    end;
    result := sb.ToString;
  finally
    sb.Free;
  end;
end;

function TJSONReader.ArrayFromToken(lo, hi: integer): IJSONArray;
var
  i, p: integer;
  value: string;
begin
  result := TJSON.NewArray;
  i := lo;
  while i < hi do
  begin
    value := fTokens[i];
    if value = '{' then // nested Object
    begin
      p := getClosingTokenIndex(i);
      result.Put(ObjectFromToken(i + 1, p - 1));
      i := findNextElement(p);
    end
    else if value = '[' then // nested Array
    begin
      p := getClosingTokenIndex(i);
      result.Put(ArrayFromToken(i + 1, p));
      i := findNextElement(p);
    end
    else // simple value
    begin
      case getType(value) of
        jtString:
          result.Put(unescapeString(StripQuotes(value)));
        jtInteger:
          result.Put(StrToInt(StripQuotes(value)));
        jtDouble:
          result.Put(StrToFloatF(StripQuotes(value)));
        jtBoolean:
          result.Put(StripQuotes(value) = 'true');
        jtNull:
          result.Put;
      end;
      i := findNextElement(i + 1);
    end;
  end;
end;

function TJSONReader.findNextElement(aIndex: integer): integer;
var
  i: integer;
  s: string;
begin
  result := fTokens.Count;
  for i := aIndex + 1 to fTokens.Count - 1 do
  begin
    s := fTokens[i];
    if (s <> '}') and (s <> ']') and (s <> ',') then
      exit(i);
  end;
end;

function TJSONReader.getClosingTokenIndex(aIndex: integer): integer;
const
  openingBraces = ['{', '['];
  closingBraces = ['}', ']'];
var
  ch: Char;
  level: integer;
begin
  ch := fTokens[aIndex][1];
  inc(aIndex);
  level := 1;
  if CharInSet(ch, openingBraces) then
  begin
    while level > 0 do
    begin
      if CharInSet(fTokens[aIndex][1], openingBraces) then
        inc(level)
      else if CharInSet(fTokens[aIndex][1], closingBraces) then
        dec(level);
      inc(aIndex);
    end;
  end
  else
    raise JSONException.Create('Expected: "{" or "[", Actual: ' + ch);
  result := aIndex - 1;
end;

function TJSONReader.findQuotationMark(const aText: string; aIndex: integer): integer;
begin
  while aIndex <= length(aText) do
  begin
    if aText[aIndex] = '\' then
      inc(aIndex, 2) // skip escaped quotation marks
    else if aText[aIndex] = '"' then
    begin
      exit(aIndex)
    end
    else
      inc(aIndex); // skip everything else
  end;
  raise JSONException.Create('closing quotation mark not found');
end;

function TJSONReader.getType(const aText: string): TJSONType;
begin
  if isObject(aText) then
    result := jtObject
  else if isArray(aText) then
    result := jtArray
  else if isString(aText) then
    result := jtString
  else if isBoolean(aText) then
    result := jtBoolean
  else if isInteger(aText) then
    result := jtInteger
  else if isDouble(aText) then
    result := jtDouble
  else if isNull(aText) then
    result := jtNull
  else
    raise JSONException.Create('Couldn''t identify valuetype of "' + aText + '"');
end;

function TJSONReader.isArray(const aText: string): boolean;
begin
  result := (aText[1] = '[') and (aText[length(aText)] = ']');
end;

function TJSONReader.isBoolean(const aText: string): boolean;
begin
  result := (aText = 'true') or (aText = 'false');
end;

function TJSONReader.isControlCharacter(aChar: Char): boolean;
const
  controlChars = ['{', '}', '[', ']', ':', ','];
begin
  result := CharInSet(aChar, controlChars);
end;

function TJSONReader.isDouble(const aText: string): boolean;
var
  d: double;
  fs: TFormatSettings;
begin
  fs := TFormatSettings.Create('en-US');
  fs.DecimalSeparator := '.';
  result := TryStrToFloat(aText, d, fs);
end;

function TJSONReader.isInteger(const aText: string): boolean;
var
  i: integer;
begin
  result := TryStrToInt(aText, i);
end;

function TJSONReader.isNull(const aText: string): boolean;
begin
  result := aText = 'null';
end;

function TJSONReader.isObject(const aText: string): boolean;
begin
  result := (aText[1] = '{') and (aText[length(aText)] = '}');
end;

function TJSONReader.isString(const aText: string): boolean;
begin
  if length(aText) > 1 then
    result := (aText[1] = '"') and (aText[length(aText)] = '"')
  else
    result := false;
end;

function TJSONReader.ObjectFromToken(lo, hi: integer): IJSONObject;
var
  i, p: integer;
  key, value: string;
begin
  result := TJSON.NewObject;
  i := lo;
  while i < hi do
  begin
    if not isString(fTokens[i]) then
      raise JSONException.Create('Invalid Key: ' + fTokens[i]);
    key := StripQuotes(fTokens[i]);
    key := unescapeString(key);
    value := fTokens[i + 2];
    if value = '{' then // nested Object
    begin
      p := getClosingTokenIndex(i + 2);
      result.Put(key, ObjectFromToken(i + 3, p - 1));
      i := findNextElement(p);
    end
    else if value = '[' then // nested Array
    begin
      p := getClosingTokenIndex(i + 2);
      result.Put(key, ArrayFromToken(i + 3, p));
      i := findNextElement(p);
    end
    else // simple value
    begin
      case getType(value) of
        jtString:
          result.Put(key, unescapeString(StripQuotes(value)));
        jtInteger:
          result.Put(key, StrToInt(StripQuotes(value)));
        jtDouble:
          result.Put(key, StrToFloatF(StripQuotes(value)));
        jtBoolean:
          result.Put(key, StripQuotes(value) = 'true');
        jtNull:
          result.Put(key);
      end;
      i := findNextElement(i + 3);
    end;
  end;
end;

function TJSONReader.readArray(const aText: string): IJSONArray;
var
  s: string;
begin
  s := TJSONIOHelper.RemoveWhiteSpace(aText);
  if (s = '[]') or (s = '') then
    result := NewJSONArray
  else if getType(s) <> jtArray then
    raise JSONException.Create('Not an array: ' + aText)
  else
  begin
    fTokens := Tokenize(s);
    result := ArrayFromToken(findNextElement(0), fTokens.Count - 1);
    fTokens.Free;
  end;
end;

function TJSONReader.readObject(const aText: string): IJSONObject;
var
  s: string;
begin
  s := TJSONIOHelper.RemoveWhiteSpace(aText);

  if (s = '{}') or (s = '') then
    result := NewJSONObject
  else if getType(s) <> jtObject then
    raise JSONException.Create('Not an object: ' + s)
  else
  begin
    fTokens := Tokenize(s);
    result := ObjectFromToken(1, fTokens.Count - 2);
    fTokens.Free;
  end;
end;

function TJSONReader.StripQuotes(const aText: string): string;
begin
  result := aText;
  if result[1] = '"' then
    delete(result, 1, 1);
  if result[length(result)] = '"' then
    delete(result, length(result), 1);
end;

function TJSONReader.StrToFloatF(const aText: string): double;
var
  f: TFormatSettings;
begin
  f := TFormatSettings.Create('en-US');
  f.DecimalSeparator := '.';
  result := StrToFloat(aText, f);
end;

function TJSONReader.Tokenize(aText: string): TList<string>;
var
  token: string;
  i, p: integer;
begin
  result := TList<string>.Create;
  i := 1;
  token := '';
  while i <= length(aText) do
  begin
    if aText[i] = '"' then // each string is a token
    begin
      p := findQuotationMark(aText, i + 1);
      token := copy(aText, i, p - i + 1);
      i := p;
    end
    else if not isControlCharacter(aText[i]) then // each number or bool or null is a token
    begin
      token := token + aText[i];
    end
    else // control characters are tokens
    begin
      if token <> '' then
        result.Add(token);
      token := aText[i];
      result.Add(token);
      token := '';
    end;
    inc(i);
  end;
end;

end.
